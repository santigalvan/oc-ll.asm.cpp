#IMAGEN 500x500
echo "PROCESANDO IMAGEN 500x500.."
magick convert -depth 8 -size 500x500 imagenes/imagen1.png img1.rgb
magick convert -depth 8 -size 500x500 imagenes/imagen2.png img2.rgb
magick convert -depth 8 -size 500x500 imagenes/mascara.png mask3.rgb

gcc main.c enmascarador.o -o test
./test "img2.rgb" "img1.rgb" "mask3.rgb"

magick convert -depth 8 -size 500x500 salida_asm.rgb imagenes/final_asm500x500.png
magick convert -depth 8 -size 500x500 salida_c.rgb imagenes/final_c500x500.png

#IMAGEN 1240x720
#echo "PROCESANDO IMAGEN 1240x720.."
#magick convert -depth 8 -size 1240x720 imagenes/img7201.jpg img1.rgb
#magick convert -depth 8 -size 1240x720 imagenes/img7202.jpg img2.rgb
#magick convert -depth 8 -size 1240x720 imagenes/mask720.png mask3.rgb

#gcc main.c enmascarador.o -o test
#./test "img2.rgb" "img1.rgb" "mask3.rgb"

#magick convert -depth 8 -size 1240x720 salida_asm.rgb imagenes/final_asm1240x720.png
#magick convert -depth 8 -size 1240x720 salida_c.rgb imagenes/final_c1240x720.png

#IMAGEN 1920x1080
echo "PROCESANDO IMAGEN 1920x1080.."
magick convert -depth 8 -size 1920x1080 imagenes/img1.jpg img1.rgb
magick convert -depth 8 -size 1920x1080 imagenes/img2.jpg img2.rgb
magick convert -depth 8 -size 1920x1080 imagenes/mask1080.png mask3.rgb

gcc main.c enmascarador.o -o test
./test "img2.rgb" "img1.rgb" "mask3.rgb"

magick convert -depth 8 -size 1920x1080 salida_asm.rgb imagenes/final_asm1080.png
magick convert -depth 8 -size 1920x1080 salida_c.rgb imagenes/final_c1080.png

#IMAGEN 2k
#echo "PROCESANDO IMAGEN 2540x1440.."
#magick convert -depth 8 -size 2540x1440 imagenes/imagen2k1.jpg img1.rgb
#magick convert -depth 8 -size 2540x1440 imagenes/imagen2k2.jpg img2.rgb
#magick convert -depth 8 -size 2540x1440 imagenes/mask2k.png mask3.rgb
#
#gcc main.c enmascarador.o -o test
#./test "img2.rgb" "img1.rgb" "mask3.rgb"
#
#magick convert -depth 8 -size 2540x1440 salida_asm.rgb imagenes/final_asm2k.png
#magick convert -depth 8 -size 2540x1440 salida_c.rgb imagenes/final_c2k.png
#
##IMAGEN 4k
#echo "PROCESANDO IMAGEN 3840x2160.."
#magick convert -depth 8 -size 3840x2160 imagenes/imagen4k1.png img1.rgb
#magick convert -depth 8 -size 3840x2160 imagenes/imagen4k2.jpg img2.rgb
#magick convert -depth 8 -size 3840x2160 imagenes/mask4k.png mask3.rgb

#gcc main.c enmascarador.o -o test
#./test "img2.rgb" "img1.rgb" "mask3.rgb"

#magick convert -depth 8 -size 3840x2160 salida_asm.rgb imagenes/final_asm4k.png
#magick convert -depth 8 -size 3840x2160 salida_c.rgb imagenes/final_c4k.png

rm img1.rgb
rm img2.rgb
rm mask3.rgb
rm salida_asm.rgb
rm salida_c.rgb