section .data

section .text
    global enmascarar_asm    

enmascarar_asm:
	;ENTER
    PUSH EBP   
    MOV EBP, ESP 
    
	;LIMPIEZA
	XOR EAX, EAX
	XOR EBX, EBX
	XOR ECX, ECX
	XOR EDX, EDX

    ;VALORES INICIALES
    MOV EAX, DWORD[EBP+20] ;TRAEMOS EL LENGTH
    ADD EBX, 8
    DIV EBX
    MOV ECX, EAX
    
    CICLO:
        MOV EAX, ECX
        MOV EBX, 8
        MUL EBX
        SUB EAX, 8

        ;TRAEMOS LA PRIMER IMAGEN
        MOV EBX,[EBP+8]
        ADD EBX, EAX
        MOVQ MM0, [EBX]
        
        ;TRAEMOS LA SEGUNDA IMAGEN
        MOV EBX, [EBP+12]
        ADD EBX, EAX
        MOVQ MM1, [EBX]
        
        ;TRAEMOS LA MASCARA
        MOV EBX, [EBP+16]
        ADD EBX, EAX
        MOVQ MM2, [EBX]

        ; Operaciones para enmascarar
        PAND MM1, MM2   ;Elimino de la segunda imagen lo que no corresponde
        PANDN MM2, MM0  ;Elimino de la primera imagen lo que no corresponde
        POR MM1, MM2    ;Combino ambas imagenes
        
        ; Guardo en imagen 1 el resultado
        mov EBX, [EBP+8] ; img1
        ADD EBX, EAX
        MOVQ [EBX], MM1

        LOOP CICLO

    EMMS ; restaura la pila
    
    ; Generales
    mov ESP, EBP ; restaura ESP
    pop EBP ; restaura EBP
        
ret