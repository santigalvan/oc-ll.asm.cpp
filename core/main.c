#include <stdio.h>
#include <stdlib.h>
#include <time.h>

extern void enmascarar_asm(unsigned char *imagen1, unsigned char *imagen2, unsigned char *imagenMascara, int longitudImagen);

void delay(int milli_seconds ){
    clock_t start_time = clock();  
    while (clock() < start_time + milli_seconds); 
}
  

char * convertirImagenAArray(char *path){
    FILE *fileptr;
    char *buffer;
    long filelen;

    fileptr = fopen(path, "rb");                        // Open the file in binary mode
    fseek(fileptr, 0, SEEK_END);                        // Jump to the end of the file
    filelen = ftell(fileptr);                           // Get the current byte offset in the file
    rewind(fileptr);                                    // Jump back to the beginning of the file

    buffer = (char *)malloc((filelen+1)*sizeof(char));  // Enough memory for file + \0
    fread(buffer, filelen, 1, fileptr);                 // Read in the entire file
    fclose(fileptr);                                    // Close the file
    return buffer;
}

int obtenerLongitudImagen(char *path){
    FILE *fileptr;
    char *buffer;
    long filelen;

    fileptr = fopen(path, "rb");                        // Open the file in binary mode
    fseek(fileptr, 0, SEEK_END);                        // Jump to the end of the file
    filelen = ftell(fileptr);                           // Get the current byte offset in the file
    return filelen;
}

int generarArchivoSalidaRGB(char *vector, int largo, const char *ruta) {
	FILE *archivo = fopen(ruta, "wb");
	fwrite(vector, sizeof(char), largo, archivo);
	fclose(archivo);
    return 0;
}

void enmascarar_c(unsigned char *imagen1, unsigned char *imagen2, unsigned char *imagenMascara, int longitudImagen){
    clock_t tiempo_comienza_c = clock(); 
    int i;
	char imagenRet[longitudImagen];
	for (i=0; i<longitudImagen; i++){
        if(imagenMascara[i] == 0){
            imagenRet[i] = imagen1[i];
        }else{
            imagenRet[i] = imagen2[i];
        }
    }
    clock_t tiempo_termina_c = clock();
	double tiempo_c = tiempo_termina_c - tiempo_comienza_c;
    printf("El tiempo de ejecucion en C es: %f ms\n", tiempo_c);
    generarArchivoSalidaRGB(imagenRet, longitudImagen, "salida_c.rgb");
    
}

int main(int argc, char *argv[]){
    char *pathImagen1 =  argv[1];
    char *pathImagen2 =  argv[2];
    char *pathMascara =  argv[3];

    char * arrayImage1 = convertirImagenAArray(pathImagen1);
    char * arrayImage2 = convertirImagenAArray(pathImagen2);
    char * arrayMask = convertirImagenAArray(pathMascara);

    //Asumimos que todas las imagenes tienen la misma longitu
    int longitudImagenes = obtenerLongitudImagen(pathImagen1);

    enmascarar_c(arrayImage1, arrayImage2, arrayMask, longitudImagenes);
    
    clock_t tiempo_comienza_asm = clock(); 
    enmascarar_asm(arrayImage1, arrayImage2, arrayMask, longitudImagenes);
    clock_t tiempo_termina_asm = clock(); 
	double tiempo_asm = tiempo_termina_asm - tiempo_comienza_asm;
    printf("El tiempo de ejecucion en ASM es: %f ms\n", tiempo_asm);
    printf(".........................................\n");

    generarArchivoSalidaRGB(arrayImage1, longitudImagenes, "salida_asm.rgb");

    return 0;
}