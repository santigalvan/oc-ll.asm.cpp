global enmascarar_asm

section .data
mensajeInformativo db "Imagen1: %p, Imagen2 %p, Mask: %p, Len: %d ",10,0
msgmask db "ESI: %dd",10,0
msglen db "ECX: %d",10,0
msgmedio db "EAX: %p , EAX+8*ECX: %p",10,0
imagen1 dd 0
imagen2 dd 0
aux dd 0
color dw 0
mascara dd 0
len dd 0

msg     db  'Hello, world!',0xa   

section .text
    extern printf

enmascarar_asm:
    push EBP
    mov  EBP,ESP

    ;Obtengo Parametros
    XOR EBX, EBX
    MOV EBX, [EBP+8]
    MOV [imagen1], EBX     ;puntero a Imagen1
    
    XOR EBX, EBX
    MOV EBX, [EBP+12]
    MOV [imagen2], EBX    ;puntero a Imagen2
    
    XOR EAX, EAX
    MOV EAX, [EBP+16]
    MOV [mascara], EAX    ;puntero a Mascara
   
    XOR EAX, EAX
    MOV EAX, [EBP+20]
    MOV [len], EAX    ;cantidad de bytes
    
    ;Para Imrprimir
    ;push dword [len] ;pusheo para imprimir
    ;push dword [mascara]
    ;push dword [imagen2]
    ;push dword [imagen1]
    ;push dword mensajeInformativo

    ;call printf
    ;add esp, 20
   ;Termina impresion comienza otra impresion
   
   ;Pruebo el ECX para ver si vale 0
    XOR EAX, EAX
    XOR EBX, EBX
    XOR ECX, ECX
    XOR EDX, EDX

    MOV ECX, 0
    ;MOV EAX, [imagen1]
    MOV ESI, [imagen1]
    ;MOV EBX, [imagen2]
    ;MOV EDX, [mascara]
LOOP:

    ;CMP ECX, 20000
    ;CMP [len], ECX
    ;MOV [ESI], DWORD -5650565
    ;push EBP
    ;mov  EBP,ESP
    ;PUSH DWORD[ESI]
    ;PUSH msgmask
    ;call printf

    MOV EAX, ESI ;X465O
    MOV [aux], EAX
    MOV DWORD[aux], 0
    ADD ESI, 8
    
    ;CMP [ESI], DWORD 0
    ;JE ESBLANCO
    ;MOV DWORD[ESI], 10
    ;ESBLANCO:
    ;ADD ESI, 8
    ;MOV EAX, DWORD[EBX]
    INC ECX
    CMP ECX, 1000
    JE FIN
    JMP LOOP

    ;JE FIN
    
    ;CMP [ESI], 0b
    
    
    ;LEA reg,mem
    ;Almacena la dirección efectiva del operando de memoria en un registro.
    ;Operación: reg <- dirección mem
    ;MOV DWORD[EAX], EBX
    ;LEA EBX, [EBX+ECX*8]
    ;MOV EBX, [EBX+ECX*8]
    ;MOV EDX, [EDX+ECX*8]

    ;MP EDX, 0
    ;MOV EAX, [EBX+8*ECX]

    
    ;INC ECX  

    

FIN:    
    push ECX
    push msglen
    call printf
    LEAVE
    RET
