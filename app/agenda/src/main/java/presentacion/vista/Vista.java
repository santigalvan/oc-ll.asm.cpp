package presentacion.vista;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.*;

public class Vista
{
	private JFrame frame;
	private JButton btnSubirImagen1;
	private JButton btnSubirImagen2;
	private JButton btnCombinar;
	private JButton btnDescargar;
    private JLabel image1;

	public Vista() 
	{
		super();
		initialize();
	}


	private void initialize() 
	{
		frame = new JFrame();
		frame.setBounds(0, 0, 900, 600);
//		frame.setExtendedState(MAXIMIZED_BOTH);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 600, 600);
		frame.getContentPane().add(panel);
		panel.setLayout(null);

        btnSubirImagen1 = new JButton("Subir Imagen 1");
		btnSubirImagen1.setBounds(10, 450, 200, 25);
		panel.add(btnSubirImagen1);

        btnSubirImagen2 = new JButton("Subir Imagen 2");
        btnSubirImagen2.setBounds(208, 450, 200, 25);
        panel.add(btnSubirImagen2);

        btnCombinar = new JButton("Combinar");
        btnCombinar.setBounds(250, 300, 200, 25);
        panel.add(btnCombinar);

        btnDescargar = new JButton("Descargar");
        btnDescargar.setBounds(250, 530, 100, 25);
        panel.add(btnDescargar);

        image1 = new JLabel();
        image1.setBounds(10, 10, 400, 200);

    }
	
	public void show()
	{
		this.frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.frame.addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
		        int confirm = JOptionPane.showOptionDialog(
		             null, "Estas seguro que quieres salir de la Agenda!?", 
		             "Confirmacion", JOptionPane.YES_NO_OPTION,
		             JOptionPane.QUESTION_MESSAGE, null, null, null);
		        if (confirm == 0) {
		           System.exit(0);
		        }
		    }
		});
		this.frame.setVisible(true);
	}
	
	public JButton getBtnSubirImagen1(){ return btnSubirImagen1; }

	public JButton getBtnSubirImagen2()
	{
		return btnSubirImagen2;
	}
	
	public JButton getBtnCombinar()
	{
		return btnCombinar;
	}

	public JButton getBtnbtnDescargar(){ return btnDescargar;}

    public JFrame getFrame(){ return frame;}

    public JLabel getImage1() {return image1;}

}
