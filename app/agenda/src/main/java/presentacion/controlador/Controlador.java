package presentacion.controlador;

import presentacion.vista.Vista;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;


public class Controlador
{
		private Vista vista;

		public Controlador(Vista vista)
		{
			this.vista = vista;
			this.vista.getBtnSubirImagen1().addActionListener(accion        -> cargarImagen1());
			this.vista.getBtnSubirImagen2().addActionListener(accion        -> cargarImagen2());
			this.vista.getBtnCombinar().addActionListener(accion            -> combinar());
			this.vista.getBtnbtnDescargar().addActionListener(accion        -> descargar());
		}

        private void descargar() {
            System.out.println("Descargar");
        }

        private void combinar() {
            System.out.println("Combinar");
        }

        private void cargarImagen1() {
            File rutaImagen1 = loadFile();
            vista.getImage1().setIcon( new ImageIcon(rutaImagen1.getAbsolutePath()));
            System.out.println("rutaImagen1 = " + rutaImagen1.getAbsolutePath());
        }

        private void cargarImagen2() {
            File rutaImagen2 = loadFile();
            System.out.println("rutaImagen2 = " + rutaImagen2.getAbsolutePath());
        }

        private File loadFile() {
            File archivo = null;
		    JFileChooser fc = new JFileChooser();
            FileNameExtensionFilter fi = new FileNameExtensionFilter("png", "png");
            fc.setFileFilter(fi);
            fc.setDialogTitle("Cargar Imagen");
            if(fc.showOpenDialog(vista.getFrame()) == JFileChooser.APPROVE_OPTION){
                archivo = new File(fc.getSelectedFile().toString());
            }
            return archivo;
        }

        public void inicializar(){
			this.vista.show();
		}


}
